Rails.application.routes.draw do
  root to: 'noodles#index'

  resource :noodles do
    post :cook
  end
end
