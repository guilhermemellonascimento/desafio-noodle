require 'rails_helper'

RSpec.describe Noodle, type: :model do
  describe '#calculate' do
    context 'with valid values' do
      describe '1 minute noodle' do
        let(:cooking_time) { 1 }

        it 'should be cooked in 3 minutes with 2 and 3 hourglasses' do
          noodle = Noodle.new(cooking_time, 2, 3)
          noodle.calculate

          expect(noodle.cooked).to be true
          expect(noodle.cooking_time).to eq(3)
        end
      end

      describe '3 minute noodle' do
        let(:cooking_time) { 3 }

        it 'should be cooked in 7 minutes with 4 and 7 hourglasses' do
          noodle = Noodle.new(cooking_time, 4, 7)
          noodle.calculate

          expect(noodle.cooked).to be true
          expect(noodle.cooking_time).to eq(7)
        end

        it 'should be cooked in 10 minutes with 5 and 7 hourglasses' do
          noodle = Noodle.new(cooking_time, 5, 7)
          noodle.calculate

          expect(noodle.cooked).to be true
          expect(noodle.cooking_time).to eq(10)
        end
      end

      describe '4 minute noodle' do
        let(:cooking_time) { 4 }

        it 'should be cooked in 20 minutes with 5 and 8 hourglasses' do
          noodle = Noodle.new(cooking_time, 5, 8)
          noodle.calculate

          expect(noodle.cooked).to be true
          expect(noodle.cooking_time).to eq(20)
        end
      end
    end

    context 'with invalid values' do
      describe '1 minute noodle' do
        let(:cooking_time) { 1 }

        it 'should not be cooked with 2 and 2 hourglasses' do
          noodle = Noodle.new(cooking_time, 2, 2)
          noodle.calculate

          expect(noodle.cooked).to be false
        end
      end

      describe '3 minute noodle' do
        let(:cooking_time) { 3 }

        it 'should not be cooked with 3 and 1 hourglasses' do
          noodle = Noodle.new(cooking_time, 3, 1)
          noodle.calculate

          expect(noodle.cooked).to be false
        end

        it 'should not be cooked with 4 and 6 hourglasses' do
          noodle = Noodle.new(cooking_time, 4, 6)
          noodle.calculate

          expect(noodle.cooked).to be false
        end
      end
    end
  end
end
