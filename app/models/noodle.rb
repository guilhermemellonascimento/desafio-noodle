class Noodle
  attr_accessor :cooking_time, :cooked

  def initialize(time, amp1, amp2)
    @time = time.to_i
    @amp1 = amp1.to_i
    @amp2 = amp2.to_i
  end

  def calculate
    # aux vars
    x, y = 1, 1

    # check equal values
    if @amp1 == @amp2
      @cooked = false
      return
    end

    while ((@amp1 * x - @amp2 * y).abs != @time) do
      # invalid data
      if (x * @amp1 == y * @amp2)
        @cooked = false
        return
      end

      # logic
    	if (x * @amp1 < y * @amp2)
        x += 1
    	else
        y += 1
    	end
    end

    @cooked = true
    @cooking_time = [x * @amp1, y * @amp2].max # multiply aux vars and amps to get max cooking time
  end
end
