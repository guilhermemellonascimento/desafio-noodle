module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger" }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(
        content_tag(:div) do
          content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} text-center") do
            concat message
          end
        end
      )
    end

    nil
  end
end
