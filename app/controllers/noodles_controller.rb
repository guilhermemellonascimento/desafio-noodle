class NoodlesController < ApplicationController
  def cook
    noddle = Noodle.new(params[:tempo], params[:amp1], params[:amp2])
    noddle.calculate

    if noddle.cooked
      flash[:success] = "O miojo deve ficar pronto em #{noddle.cooking_time} minutos :)"
    else
      flash[:error] = "Não é possível cozinhar o miojo :("
    end

    redirect_to action: :index
  end
end
